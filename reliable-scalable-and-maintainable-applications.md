# Reliable, Scalable, and Maintainable Applications

- **data-intensive** vs. **compute-intensive**
- database
- cache
- search index
- stream processing
  - send a message to another process, to be handled asynchronously
- batch processing
  - periodically crunch a large amount of accumulated data
- **Reliability**
- **Scalability**
- **Maintainability**

## Reliability

- Expectations:
  - the application performs the function that the user expected
  - can tolerate user mistakes or user using the app in unexpected ways
  - system prevents any unauthorized access and abuse
  - performance is good enough for the required use case, under expected
    load/data volume
- fault-tolerant/resilient
- fault !== failure
  - fault: one component of the system deviates from its spec
  - failure: when the system as a whole stops providing the required service to
    user
- _Design fault-tolerance mechanisms that prevent faults from causing failures_
- many critical bugs are actually due to poor error handling
- process isolation
- allow processes to crash and restart
- decouple the places where people make the most mistakes from the places where
  they can cause failures
  - fully featured non-production _sandbox_ environments
- **telemetry**

## Scalability

- **scalability**: a system's ability to cope with increased load
- **load parameters**
  - requests per second to a server
  - the ratio of reads to writes in a db
  - number of simultaneously active users in a chat room
  - hit rate on a cache
- **throughput**:
  - number of records the system can process per second, or
  - total time it takes to run a job on a dataset of a certain size
- in online systems, **response time** is more important
- **latency** vs. **response time**
  - response time: what the client sees
    - service time + network delays + queueing delays
  - latency: the duration that a request is waiting to be handled
- think of response time as a _distribution_ of values
- to measure response time:
  - use **mean**
  - use **percentile**
    - the median is called **p50**
    - p95, p99, and p999
- **tail latency**: high percentiles of response times
  - tail latency amplification
- **SLO** - service level objectives
- **SLA** - service level agreements
- **head-of-line** blocking
- **scaling up** (vertical scaling) vs. **scaling out** (horizontal scaling)
- **shared-nothing architecture**: distributing load across multiple machines
- **elastic**: automatically add computing resources when a load increase
  detected

## Maintainability

- Three design principles for software systems:
  - **operability**
  - **simplicity**
  - **evolvability**
- **abstraction** - one of the best tools for removing accidental complexity
