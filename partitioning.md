# Partitioning

- break the data up into **partitions**
- a.k.a. **sharding**
- each piece of data (record, row, or document) belongs to _exactly one_
  partition
- partition because of **scalability**

## Partitioning and Replication

- a node may store more than one partition

## Partitioning of Key-Value Data

- **skewed**
- **hot spot**: a partition with disproportionately high load
- To avoid hot spot:
  - assign records to nodes **randomly**

### Partitioning by Key Range

- assign a continuous range of keys to each partition
- Downside:
  - certain access patterns can lead to hot spots
  - prefix the key with something else

### Partitioning by Hash of Key

- use a hash function to determine the partition for a given key
- a good hash function takes skewed data and makes it uniformly distributed
- assign each partition a range of hashes (instead of a range of keys)
- the partition boundaries can be evenly spaced or can be chosen pseudorandomly
  (consistent hashing)
- Consistent Hashing
  - a way of distributing load across an internet-wide system of caches such as
    CDN
  - here, _consistent_ describes a particular approach to rebalancing
- Downside:
  - we cannot do efficient range queries
- Cassandra achieves a compromise between range-based and hash-based:
  - a table can be declared with a **compound primary key** consisting of
    several columns
  - only the first part of that key is hashed to determine partition
  - other columns are used as a concatenated index for sorting the data in teh
    SSTables
  - a query cannot search for a range within the first column of a compound key
  - BUT it can perform an efficient range scan over the other columns of the
    key if it specifies a fixed value for the first column
  - elegant for one-to-many relationships
  - e.g. for a social network `(user_id, update_timestamp)` can be efficiently
    queried for a particular user's updates

### Skewed Workloads and Relieving Hot Spots

- Hashing the key cannot completely avoid hot spots
  - e.g. an celebrity on a social network
- A simple technique:
  - add a random number to the beginning/end of the key

## Partitioning and Secondary Indexes

- Secondary index is a way to search for occurrences of a particular value
- very useful for data modeling
- Problem is secondary indexes don't map neatly to partitions

### Partitioning Secondary Indexes by Document

- Each partition maintains its own secondary indexes, covering documents only
  in that partition
- a.k.a. **local index**
- But you would need to send queries to _all_ partitions
  - **scatter/gather**
  - prone to tail latency amplification
  - but widely used by MongoDB, Cassandra, Elasticsearch, etc

### Partitioning Secondary Indexes by Term

- **global index**
- a global index must also be partitioned
  - can be partitioned differently from the primary key index
  - **term-partitioned**
- reads more efficient
- writes are slower and more complicated
  - a write to a single document may now affect multiple partitions of the
    index
- In practice, updates to global secondary indexes are often async
  - if you read the index shortly after a write, the change just made may not
    yet be reflected in the index

## Rebalancing Partitions

- **rebalancing**: the process of moving load from one node to another
- rebalancing rules:
  - after rebalancing, the load (data storage, read, write) should be shared
    fairly between nodes
  - while rebalancing, db should continue to accept reads/writes
  - no more data than necessary should be moved between nodes

### Strategies for Rebalancing

- **DO NOT**:
  - hash mod N
  - if number of nodes `N` changes, most keys will need to be moved
- Fixed number of partitions
  - create many more partitions than there are nodes
  - assign several partitions to each node
  - if a node is added to the cluster, the new node can _steal_ a few
    partitions from every existing node until partitions are fairly
    distributed once again
  - only entire partitions are moved between nodes
  - number of partitions does **NOT** change
  - assignment of keys to partitions does **NOT** change
  - number of partitions is usually fixed
  - a fixed number of partitions is operationally simpler
- Dynamic partitioning
  - for key range-partitioned db such as RethinkDB
  - split and merge to keep the size of each partition between some fixed min
    and max
  - Advantage:
    - number of partitions adapts to the total data volume
  - Caveat
    - an empty db starts off with a single partition, since no priori info
      about where to draw the partition boundaries
    - to solve - **pre-splitting**
      - requires you to know already what the key distribution is going to look
        like
- Partitioning proportionally to nodes
  - a fixed number of _partitions per node_

## Request Routing

- **service discovery**
- Different approaches:
  - Allow clients to contact any node (via round-robin load balancer)
  - Send all requests to a routing tier first
    - this routing tier is just a partition-aware load balancer
  - Require that clients be aware of teh partitioning and assignment of
    partitions
- How does the routing-decision making component learn about changes in the
  assignment of partitions to nodes?
  - Important that all participants agree
  - many distributed data systems rely on a separate coordination service such
    as ZooKeeper
- Cassandra and Riak use **gossip protocol**
  - puts more complexity in db nodes but avoids dependency on external
    coordination service

### Parallel Query Execution

- **massive parallel processing** (MPP)
