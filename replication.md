# Replication

- **Replication**: keep a copy of the same data on multiple machines that are
  connected via a network
- Reasons:
  - keep data geographically close to users
  - allow the system to continue working even if some parts of it have failed
    - high availability
  - scale out the number of machines that can serve read requests - high
    throughput
- Difficult to handle _changes_ to the replicated data
- Three algorithms to replicate changes between nodes:
  - **single-leader**
  - **multi-leader**
  - **leaderless**

## Leaders and Followers

- Every write to the db needs to be processed by every **replica**
- Most common solution: **leader-based replication**
  - a.k.a. **active/passive** or **master-slave** replication
- How leader-based replication works:
  1. One of the replicas designated as **leader** (or master or primary); when
     client wants to write data to db, they must send requests to the leader,
     which first writes new data to its local storage
  2. The other replicas are **followers** (or read replicas, slaves,
     secondaries, or hot standbys); whenever the leader writes new data to its
     storage, it also sends the data change to all followers as part of
     a **replication log** or **change stream**
  3. When client wants to read, it can query either the leader or any of the
     followers

### Synchronous vs. Asynchronous Replication

- whether the replication happens **synchronously** or **asynchronously**
- Synchronous
  - the leader waits until follower 1 has confirmed that it received the write
    before reporting success to the user & before making the write visible to
    other clients
- Asynchronous
  - leader sends the message to follower, but does not wait for a response from
    the follower
- Advantage of synchronous replication: the follower is guaranteed to have an
  up-to-date copy of the data that's consistent with leader
- Disadvantage of synchronous replication: if follower doesn't respond, the
  write cannot be processed - the leader must block all writes and wait
- Thus not practicable for all followers to be synchronous
- In practice, if synchronous replication:
  - **one** of the followers is synchronous and the rest are asynchronous
  - if the synchronous follower becomes unavailable/slow, one of the rest
    asynchronous followers becomes synchronous
  - **semi-synchronous**
- Often, leader-based replication is completely asynchronous
  - if leader fails and is not recoverable, any writes not replicated to
    followers are lost
  - write not guaranteed to be durable
- Research
  - **chain replication**
  - a strong connection between consistency of replication and **consensus**

### Setting Up New Followers

- Without downtime
  1. Take a screenshot of the leader's db - without taking a lock on the entire
     db, if possible
  2. Copy the snapshot to the new follower node
  3. the follower connects to the leader and requests all changes _since_ the
     snapshot
     - it requires that the snapshot is associated with an exact position in
       the leader's replication log
     - a.k.a **log sequence number** in PostgreSQL
  4. When the follower has processed the backlog of data since the snapshot,
     it has **caught up**

### Handling Node Outages

- To achieve high availability with leader-based replication
- Follower failure: Catch-up recovery
  - follower keeps a log of data changes it has received from the leader, on
    disk
  - recovers based on the log
- Leader failure: Failover
  - one of the followers needs to be promoted to be the new leader
  - clients need to be reconfigured to send requests to the new leader
  - other followers need to start consuming data changes from the new leader as
    well
- Automatic failover
  1. Determine the leader has failed - if the node hasn't responded to messages
     for, say 30 seconds
  2. Choose a new leader
     - election process (chosen by a majority of remaining repilcas)
     - or a previously elected **controller node**
     - the best candidate - the replica with the most up-to-date data changes
     - getting all nodes to agree on a new leader - a **consensus** problem
  3. Reconfigure the system to use the new leader
     - if old leader comes back, ensure it becomes a follower and recognizes
       the new leader
- Things that can go wrong
  - if asynchronous replication, the new leader may not have received all
    writes from the old leader before it failed
  - discarding writes is dangerous
  - possible that two nodes both believe that they are the leader
    - **split brain**
    - a mechanism to detect two leaders and shut down one of them
  - what's the right timeout before the leader is declared dead?

### Implementation of Replication Logs

- Statement-based replication
  - the leader logs every write request (**statement**) that it executes and
    sends it to followers
  - Problems with it:
    - Nondeterministic functions such as `NOW()` or `RAND()`
    - If statements use an autoincrementing column
    - If statements depend on existing data in the db
      - they must be executed in exactly same order on each replica
    - Statements with side effects may have other side effects occurring on
      each replica, unless the side effects are absolutely deterministic
- Write-ahead log (WAL) shipping
  - for log-structured storage engine, the log is the main place for storage;
    log segments are compacted and garbage-collected in the background
  - for B-tree, which overwrites individual disk blocks, every modification is
    first written to a write-ahead log so that the index can be restored to
    a consistent state after a crash
  - Disadvantages:
    - the log describes the data on a very low level
    - replication closely coupled to the storage engine
    - not easy to upgrade software or storage format
- Logical (row-based) log replication
  - Use different log formats for replication and for storage engine -> allows
    the replication log to be decoupled from the storage engine internals
  - a **logical log**
    - for relational db, it's a sequence of records describing writes to db
      tables on row level
  - a transaction that modifies several rows generates several such log
    records, followed by a record indicating that the transaction was
    committed
  - decoupled from db internals
  - easier for external applications to parse
- Trigger-based replication
  - Move replication up to the application layer
  - **triggers** and **stored procedures**
  - a **trigger** lets you register custom application code that is
    automatically executed when a data change (write transaction) occurs
  - greater overheads
  - more prone to bugs

## Problems with Replication Lag

- Leader-based replication requires all writes to go through a single node
  - but read-only queries can go to any replica
- In the **read-scaling** architecture,
  - increase the capacity for serving read-only requests by adding more
    followers
  - however, it only realistically works with async replication
- An application may see outdated info if reading from asynchronous follower
  - but it will eventually resolve
  - **eventual consistency**
- **replication lag**
  - the delay between a write on leader and that reflected on a follower

### Reading your own writes

- **read-after-write consistency**
  - a.k.a. **read-your-writes consistency**
- How to implement ^
  - When reading from something that the user may have modified, read from the
    leader; otherwise read from a follower
  - Track the time of last update and for certain amount of time after the last
    update, make all reads from the leader
  - The client can remember the timestamp of its most recent write, then the
    system can ensure the replica serving any reads for that user reflects
    updates at least until that timestamp
    - could be a **logical timestamp** (ordering of writes, such as sequence
      number) or the actual system clock
- Another problem: when user accessing the service from multiple devices
  - approaches requiring remembering the timestamp of the user's last update
    become difficult
    - the metadata would need to be centralized
  - if replicas distributed across different datacenters, no guarantee that
    connections from different devices will be routed to the same datacenter
    - may need to route requests from all user's devices to the same datacenter

### Monotonic reads

- possible for a user to see things _moving backward in time_
- **Monotonic reads**
  - a guarantee that this kind of anomaly does **NOT** happen
  - a _lesser_ guarantee than strong consistency
  - a _stronger_ guarantee than eventual consistency
  - user will not read older data after having previously read newer data
- To achieve
  - ensure each user always make reads from the same replica
    - maybe based on the hash of user id

### Consistent prefix Reads

- Violation of causality
- if some partitions are replicated slower than others
- **Consistent prefix reads**
  - If a sequence of writes happen in a certain order, then anyone reading
    those writes will see them appear in the same order
- One solution:
  - Make sure that any writes that are causally related to each other are
    written to the same partition
  - in some applications it cannot be done efficiently

### Solutions

- **Transactions**

## Multi-Leader Replication

- **master-master** or **active/active** replication
- Each leader simultaneously acts as a follower to other leaders
- Big downside: the same data may be concurrently modified in two different
  data centers
  - those conflicts must be resolved
- Should be avoided if possible

### Use cases

- Multi-datacenter operation
  - have a leader in _each_ datacenter
- Comparison between single-leader and multi-leader
  - Performance
    - single-leader: every write must go over the Internet to the datacenter
      with the leader
    - multi-leader: every write can be processed in the local datacenter and is
      replicated asynchronously to other datacenters - better performance
  - Tolerance of datacenter outages
    - single-leader: if the datacenter with the leader fails, failover can
      promote a follower in another datacenter to be leader
    - multi-leader: each datacenter can continue operating independently
  - Tolerance of network problems
    - single-leader: very sensitive to the problems occurring on the public
      inter-datacenter networks
    - multi-leader: better
- Clients with offline operation
  - Multi-leader replication is appropriate if there are applications that
    need to continue to work even if disconnected from the Internet
  - CouchDB
- Collaborative editing

### Handling write conflicts

- Synchronous vs. asynchronous conflict detection
  - single-leader: either blocked or failed
  - multi-leader: both writes are successful and the conflicts are detected
    asynchronously later
  - In principle, you should make conflict detection **synchronous**
- Conflict avoidance
  - if the application can ensure that all writes for a particular record go
    through the same leader, the conflicts cannot occur
- Converging toward a consistent state
  - Give each write a unique ID
    - pick the write with the highest ID as winner and throw away all others
    - if timestamp, **late write wins** (LWW)
  - Give each replica a unique ID
    - let writes that originated at a higher-numbered replica always take
      precedence over writes that originated at a lowered-numbered one
    - data losses!
  - Somehow merge the values together and then concatenate them
  - Record the conflict in an explicit data structure that preserves all info
    and write application code to resolve the conflicts
- Custom conflict resolution logic
  - Two ways:
    - On write
    - On read
      - CouchDB
  - conflict resolution usually applies at the level of an individual
    row/document, **NOT** for an entire transaction

### Multi-leader replication topologies

- **replication topology**
  - the communication paths along which writes are propagated from one node to
    another
- Three topologies:
  - circular
  - star
  - all-to-all
- All-to-all
  - most general
  - every leader sends its writes to every other leader
  - can have order issues
    - similar to causality
    - clocks cannot be trusted to be sufficiently in sync to correctly order
      these events
    - **version vectors** - a technique that can be used to order the events
      correctly
- In circular and star,
  - a write may need to pass through several nodes before reaching all replicas
  - nodes need to forward data changes they receive from other nodes
  - each node is given a unique ID to prevent infinite loops
  - If just one node fails, it can interrupt the flow of replication messages
    between other nodes

## Leaderless Replication

- Dynamo
  - **NOT** DynamoDB
  - DynamoDB is single-leader
- In some leaderless implementations, the client directly sends its writes to
  several replicas; in others, a coordinator node does this on behalf of the
  client
  - however, the coordinator does **NOT** enforce a particular ordering of
    writes

### Writing to DB when a node is down

- In leaderless configuration, failover does **NOT** exist
- If a sufficient number of nodes acknowledged the write, the write is
  considered to be successful and the failed replicas are simply ignored
- When the failed node is back up online and is read by the user, the value may
  be _stale_ (outdated)
  - To solve this:
    - read requests are also sent to several nodes in parallel
    - version numbers are used to determine which value is newer
- How does the unavailable node catch up when it comes back online?
  - Two mechanisms:
    - **Read repair**
      - client makes a read from several nodes in parallel
      - client detects any stale responses
      - client writes the newer value back to the replica with the stale value
      - works well for values frequently read
    - **Anti-entropy process**
      - a background process that constantly looks for differences between
        replicas
      - copies data from one replica to another
      - there may be a significant delay before data is copied
- Quorums for reading and writing
  - if there are `n` replicas,
  - every write must be confirmed by `w` nodes to be considered successful
  - we must query at least `r` nodes for each read
  - as long as `w + r > n`, we expect to get an up-to-date value when reading
  - reads/writes that obey this rule: **quorum reads/writes**
  - commonly, set `n` to be an odd number and `w = r = Math.ceil(n + 1) / 2`

### Limitations of Quorum Consistency

- You _could_ set `w + r <= n`
- lower latency & higher availability
- Even with `w + r > n`, still possible that stale values are returned
  - Possible scenarios:
    - a sloppy quorum is used - the `w` writes may not overlap with the `r`
      reads
    - if two writes occur concurrently, it's not clear which happened first
      - the only safe solution is to merge the concurrent writes
      - if a winner is picked based on timestamp (last write wins), writes can
        be lost due to clock skew
    - if a write happens concurrently with a write, the write may be reflected
      on only some of the replicas - undetermined whether the read returns the
      old or new value
    - if a write succeeded on some replicas but failed on others, and overall
      succeeded on fewer than `w` replicas, it's not rolled back on the
      replicas where it succeeded - if a write was reported as failed,
      subsequent reads may or may not return the value from that write
    - if a node carrying a new value fails and its data is restored from
      a replica carrying an old value, the number of replicas storing the new
      value may fall below `w`, breaking quorum condition
    - bad timing???
- In systems with leaderless replication, no fixed order in which writes are
  applied - more difficult to monitor
- Include staleness measurements in the standard set of metrics for dbs

### Sloppy Quorums and Hinted Handoff

- Leaderless replication good for high availability and low latency
  - can tolerate occasional stale reads
- But quorums (described so far) are **NOT** as fault-tolerant as they could be
  - e.g. an internet interruption
- In a large cluster, with significantly more than `n` nodes, it's likely that
  the client can connect to _some_ db nodes during the network interruption,
  just not to the nodes that it needs to assemble a quorum for a particular
  value
- Trade-off:
  - return errors to all requests for which we cannot reach a quorum of `w` or
    `r` nodes
  - accept writes anyway, and write them to some nodes that are reachable but
    are not among the `n` nodes where the value usually lives
    - called **sloppy quorum**
    - once connected resumed, writes sent to home nodes - **hinted handoff**
    - useful for increasing write availability: as long as **any** `w` nodes
      are available, the db can accept writes
    - not really quorum, but an assurance of durability
    - optional in all common Dynamo implementations

### Detecting Concurrent Writes

- Dynamo-style dbs allow several clients to concurrently write to the same key
  - conflicts will occur even if strict quorums are used
- In order to become eventually consistent, the replicas should converge toward
  the same value
- Late write wins (discarding concurrent writes)
  - by attaching a timestamp to each write
  - the only conflict reolution algo in Cassandra
  - LWW may even drop writes that are not concurrent
  - a poor choice if lost data not acceptable
  - the only safe way of using db with LWW is to ensure a key is only written
    once and thereafter as immutable
    - avoiding any concurrent updates to the same key
    - recommended to use UUID as key (in Cassandra) - giving each write op
      a unique key
- How to decide whether two ops are concurrent or not
  - A's insert happens before B's
    - B **causally dependent** on A
  - when each client starts the op, it does not know another client's op
    - no causal dependency between the ops
  - op A _happens before_ B if B knows about A, or depends on A, or builds
    upon A
  - two ops concurrent if neither happens before the other
- For defining concurrency, exact time doesn't matter
- Algo:
  - server maintains a version number for every key
    - increments the version number every time the key is written and stores it
  - when a client reads a key, the server returns all values that have not been
    overwritten, as well as the version number
  - when client writes a key, it must includes the version number from prior
    read and must merge all values received from the prior read
  - when server receives a write with a particular version number, it
    overwrites all values with that version number (or below)
    - but must keep all values with higher version number
- the above algo requires some extra work on client's side
  - clients have to clean up afterward by merging the concurrently written
    values
  - when an item is deleted, the system leaves a marker with the correct
    version number
    - a.k.a. **tombstone**
- Version vectors
  - we need to use a version number **per replica** as well as per key
  - each replica increments its own version number when processing a write and
    also keeps track of the version numbers it has seen from each of the
    other replicas
  - the collection of version numbers from all replicas is called **version
    vector**
  - **dotted version vector**
  - version vectors are sent from db to clients when values are read
  - also sent back to db when a value subsequently written
