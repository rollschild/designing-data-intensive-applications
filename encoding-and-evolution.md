# Encoding and Evolution

- **rolling upgrade** on the server side
- During an upgrade,
  - **backward compatibility**: newer code can read data that was written by
    older code
  - **forward compatibility**: older code can read data that was written by
    newer code

## Formats for encoding data

- (at least) Two representations
  - in memory, data kept in objects, structs, lists, arrays, hash tables,
    trees
    - they are optimized for efficient access and manipulation by CPU
      (typically using pointers)
  - when writing data to a file or sending over the network, you have to
    encode the data as self-contained sequence of bytes (e.g. JSON)
- We need translation between the two representations:
  - **encoding**: from in-memory representation to a byte sequence
    - a.k.a. **serialization** or **marshalling**
  - **decoding**: from byte sequence to the in-memory representation
    - a.k.a. **parsing**, **deserialization**, **unmarshalling**

### Language-specific formats

- problems:
  - the encoding is tied to a particular programming language
  - security risk: in order to restore data in the same object types, the
    decoding process needs to be able to instantiate arbitrary classes
  - versioning data
  - efficiency
    - CPU time taken to encode/decode
    - the size of the encoded structure

### JSON, XML, and Binary variants

- Problems
  - Ambiguity around encoding of numbers
    - XML and CSV cannot distinguish between a number and a string that
      happens to consist of digits
    - with large numbers
      - integers greater than `2 ^ 53` cannot be exactly represented in an IEEE
        754 double-precision floating-point number
  - JSON and XML have good support for Unicode but do **NOT** support binary
    strings (sequences of bytes without a character encoding)
    - to workaround, encode the binary data as text using Base64
      - however, this increases data size by 33%
- Binary encoding
  - if a format doesn't prescribe a schema, they need to include all the object
    field names within the encoded data

### Thrift and Protocol buffers

- binary encoding libraries
- they both require a schema
- Thrift
  - has three schemas:
    - **BinaryProtocol**
    - **CompactProtocol**
    - **DenseProtocol**
  - NO fields names
  - instead, the encoded data contains **field tags**, which are numbers
- Protocol Buffer
  - only one schema
  - similar to Thrift's CompactProtocol
- Field tags and schema evolution
  - Adding a new field
    - to maintain backward compatibility, every field you add after the initial
      deployment of the schema must be optional or have a default value
  - Removing a field
    - you can only remove a field that's optional
    - a required field can never be removed
    - you can **NEVER** use the same tag number again
- Datatypes and schema evolution
  - pay attention to data precision
    - write 64 bits to an originally 32 bits field
  - Protocol Buffer does **NOT** have a list/array datatype
    - instead it has a `repeated` marker for fields

### Avro

- writer's schema & reader's schema
- the schema does **NOT** contain any tag numbers -> friendlier to _dynamically
  generated_ schemas
- Code generation and dynamically typed languages
  - Thrift and Protocol Buffer rely on code generation
    - after a schema has been defined, you can generate code that implements
      this schema in a programming language of your choice
  - in dynamically typed languages, there is no compile-time type checker

### The merits of schemas

- Binary encodings based on schemas have the following properties:
  - much more compact since they can omit field names from the encoded data
  - the schema itself is a valuable form of documentation
  - keeping a db of schemas allows you to check forward/backward compatibility
    of schema changes, before anything is deployed
  - the ability to generate code from the schema for statically type languages

## Modes of dataflow

- Three most common ways data flows between processes:
  - via databases
  - via service calls
  - via asynchronous message passing

### Dataflow through databases

- data may be lost if an older version of code is not aware of the newer
  version of schema
- **Data outlives code**

### Dataflow through services: REST and RPC

- XMLHttpRequest made by JavaScript
- **service-oriented architecture**
  - microservices architecture
- a key design goal of a service-oriented/microservices architecture is to make
  the application easier to change and maintain by making services
  independently deployable and evolvable
- REST
  - not a protocol, but rather a design philosophy that builds upon the
    principles of HTTP
  - URL for identifying resources
  - HTTP features for cache control, authentication, and content type
    negotiation
- SOAP
  - XML based
  - aims to be independent from HTTP and avoids using most HTTP features
  - the API of a SOAP web service is described using an XML-based language
    called Web Services Description Language, WSDL
  - WSDL enables code generation
    - so that a client can access a remote service using local classes and
      method calls
    - useful in statically typed languages
- Problems with remote procedure calls (RPC)
  - the RPC model tries to make a request to a remote network service look the
    same as calling a function/method within the same process
  - this ^ approach is fundamentally flawed, since a network request is very
    different from a local function call
    - a local function call is predictable but a network request is not
    - a local function
      - returns a result
      - throws an exception
      - never returns (infinite loop or process crashes)
    - a network request has another possible outcome
      - returning without a result
    - if you retry a failed network request, it could happen that the previous
      request actually got through
      - the action being performed multiple times
      - unless you build a mechanism for deduplication (**idempotence**)
    - a network request is much slower than a function call and its latency is
      variable
    - when calling a function, references (pointers) to objects in local memory
      can be efficiently passed to it
    - client and service may be implemented in different languages, so RPC
      framework must translate datatypes from one language into another
  - gRPC supports **streams**, where a call consists of not just one request
    and one response, but a series of requests and responses over time
- Data encoding and evolution for RPC
  - RPC clients and servers can be changed and deployed independently
  - servers will be updated first
  - clients will be updated second
  - You only need:
    - backward compatibility on requests
    - forward compatibility on responses
- Versioning an API
  - for RESTful APIs, common approaches are:
    - use a version number in the URL, or
    - in the HTTP `Accept` header

### Message-passing dataflow

- **asynchronous message-passing** systems
- a client's request (called a **message**) is delivered to another process
  with low latency
- message is not sent via a direct network connection
  - but goes via an intermediary called a **message broker**
    - a.k.a. **message queue**, or **message-oriented middleware**
- Using a message broker has several advantages compared to RPC:
  - it can act as a buffer if recipient is unavailable or overloaded ->
    improves system reliability
  - can automatically resend the message to a process that has crashed ->
    prevents messages from being lost
  - avoids the sender needing to know the IP address and port number of the
    recipient
  - allows one message to be sent to several recipients
  - it logically decouples the sender from the recipient
- Message-passing communication is usually one-way:
  - a sender normally doesn't expect to receive a reply/response to its message
  - the communication pattern is **asynchronous**
    - the sender doesn't wait for the message to be delivered
- Message brokers
  - one process sends a message to a named **queue** or **topic**
  - and the broker ensures that the message is delivered to one/more
    **consumers** of or **subscribers** to that queue/topic
- a topic provides only one-way dataflow
- message brokers typically don't enforce any particular data model
- Distributed actor frameworks
  - the **actor model** is a programming model for _concurrency_ in a single
    process
  - logic is encapsulated in **actors**, rather than with threads
  - each actor typically represents one client/entity
    - it may have some local state (not shared with any other actor)
    - it communicates with other actors by sending/receiving asynchronous
      messages
  - message delivery is **NOT** guaranteed
  - in distributed actor frameworks, it's used to scale an app across multiple
    nodes
  - a distributed actor framework essentially integrates a message broker and
    the actor programming model into a single framework
