# Storage and Retrieval

## Data structures that power the database

- `tail -n 1` - show me the latest line
- appending to a file is generally very efficient
- many DBs internally use a **log**, which is an append-only data file
- **index**
  - a different data structure
  - to efficiently find the value for a particular key in DB
  - keep some additional metadata on the side
- an **index** is an _additional_ data structure that's _derived_ from the
  primary data
- maintaining additional data structures incurs overhead, especially on writes
  - any kind of index usually slows down writes
- Important trade-off:
  - well-chosen indexes speed up read queries
  - but every index slows down writes

### Hash indexes

- a simple strategy:
  - keep an in-memory hash map where every key is mapped to a byte offset in
    the data file - the location at which the value can be found
  - suitable for where the value of each key is updated frequently
  - a large number of writes per key
  - used by Bitcask
  - the hash map is kept completely in memory
- a better strategy, to avoid running out of disk space
  - break the log into segments of a certain size
  - perform **compaction** on the segments
  - can also merge several segments at the same time
    - segments are _never_ modified after written
    - the merged segment is written to a new file
  - merging & compaction happen in a background thread
  - each segment now has its own in-memory hash table
- A few issues:
  - file format
    - CSV is not the best format for a log
    - better to use a binary format
      - first encodes the length of a string in bytes
      - followed by the raw string (no need to escape)
  - deletion
    - to delete a key/value, append a special deletion record
    - when log segments are merged, the merging process would discard any
      previous values for the deleted key
  - crash recovery
    - if db restarted, the in-memory hash maps are lost
    - you can keep a snapshot of each segment's hash map on disk, to speed up
      recovery
  - partially written records
    - crash can occur any time, which may cause partially written data
    - can include checksums and ignore corrupted parts of the log
  - concurrency control
    - common practice: only one writer thread
    - but can be read by multiple read threads
- Advantages:
  - appending an merging are sequential write operations - much faster than
    random writes
    - especially on magnetic spinning-disk hard drives
  - concurrency and crash recovery are much simpler
    - segment files are append-only and immutable
  - merging old segments avoids the problem of data files getting fragmented
    over time
- Limitations:
  - the hash table _must_ fit in memory
    - if large number of keys, then NO
  - range queries are not efficient

### SSTables and LSM-Trees

#### SSTable

- **SSTable** (**Sorted String Table**) - the sequence of key-value pairs is _sorted by key_
  - new key-value pairs cannot be appended to the segment immediately
- Advantages of SSTable over log segments with hash indexes
  - merging segments is easy & efficient
  - no longer need to keep an index of _all_ keys in memory
    - still need an in-memory index for the offsets for some of the keys
    - can be sparse
    - one key for every few kilobytes of segment file is sufficient
  - possible to group the records in a block and compress it before writing to disk
    - each entry of the sparse in-memory index points at the start of a compressed block
    - reduces I/O bandwidth use
- possible to maintain a sorted structure on disk
  - for e.g. using B-Tree
- Much easier to maintain a sorted structure in memory
  - red-black tree
  - AVL tree
  - **memtable**
- How-to:
  - when a write comes in, add it to an in-memory tree data structure (red-black tree for e.g.)
  - when memtable grows to a threshold size (a few megabytes), write it out to disk as an SSTable file
    - the new SSTable file -> most recent segment of the DB
    - while the SSTable is being written out to disk, write can continue to a new memtable instance
  - when read requried,
    - look for the key in the memtable first
    - then in the most recent on-disk segment
    - then in the next-older segment
  - from time-to-time, run a merging and compaction process in the background to
    - combine segment files
    - discard overwritten/deleted values
- Issue
  - if DB crashes, most recent writes (in the memtable but not yet written to disk) are lost
  - fix/workaround:
    - keep a separate log on disk to which every write is immediately appended
    - not in sorted order
    - as soon as memtable is written out to disk, the log can be discarded

#### LSM-Tree

- algorithms in RocksDB and LevelDB
- originally called **Log-Structured Merge-Tree**
- LSM storage engines
- Performance optimizations:
  - slow to look up keys that don't exist
    - use additional **Bloom filters**
    - bloom filter: a memory-efficient data structure for approximating the
      contents of a set
- Different strategies to determine order and timing of SSTable's compaction
  and merging:
  - **size-tiered**
    - HBase
  - **level-tiered**
    - used by LevelDB and RocksDB
    - Cassandra supports both
- LSM-tree can support remarkably high write throughput

### B-Tree

- the most widely used indexing structure
- the standard index implementation in almost all relational DBs
- keeps key-value pairs sorted by key
  - efficient key-value lookups and range queries
- breaks the DB down into _fixed-size_ **blocks**, or **pages**
  - traditionally 4Kb in size
  - read/write one page at a time
- each page identified using an address or location
- **branching factor**
  - number of references to child pages in one page
- the tree remains **balanced**
- B-tree with `n` keys always has a depth of `O(logn)`
- the write operation **overwrites** a page on disk with new data
  - assuming the overwrite does **NOT** change the location of the page
- Reliability:
  - crash recovery
    - keep an additional data structure on disk: **write-ahead log** (WAL)
    - a.k.a. **redo log**
    - append-only
    - every B-tree modification must be written _before_ applied to the page
      itself
  - concurrency control
    - use **latch** (lightweight lock)
- Optimizations
  - use a copy-on-write scheme, instead of overwriting pages & maintaining
    a WAL for crash recovery
    - a modified page is written to a different location
    - a new version of the parent pages is created -> to the new location
    - also useful for concurrency control
  - save space in pages by not storing the entire key
    - abbreviate the key, with just enough information to act as boundaries
      between key ranges
    - higher branch factor
    - fewer levels
  - try to layout the tree so that leaf pages appear in sequential order on
    disk
  - each leaf page may have references to siblings
    - allows scanning keys in order without jumping back to parent pages
  - **fractal tree**
    - some log-structured ideas to reduce disk seeks

#### B-Tree vs. LSM-Tree

- LSM-Tree faster for writes
- B-Tree faster for reads
- Advantages of LSM-Tree
  - a B-tree index writes every piece of data at least twice
    - to the write-ahead log
    - to the tree page itself
  - also overhead of B-tree having to write an entire page at a time
  - some db engines overwrite same page twice
    - in order to avoid partially updated page in case of power failure
  - log-structured indexes also rewrite data multiple times due to repeated
    compaction and merging of SSTables
    - **write amplification**: one write to db causes in multiple writes to
      disk
    - in write-heavy applications, it's the performance bottleneck
  - LSM-trees _typically_ have higher write throughput than B-trees
    - especially on magnetic disks
    - sequential writes much faster than random writes
  - compressed better -> smaller files on disk than B-trees
    - especially when using leveled compaction
  - on many SSDs, the firmware internally uses log-structured algorithm to turn
    random writes into sequential writes on the underlying storage chips
- Downsides of LSM-trees
  - compaction process can _sometimes_ interfere with the performance of
    ongoing reads/writes
    - at higher percentiles response time of queries to log-structured db
      engines can _sometimes_ be quite high
    - B-trees more predictable
  - disk's finite write bandwidth needs to be shared between the initial write
    (logging/flushing memtable to disk) and the compaction threads running in
    the background
  - it can happen that compaction cannot keep up with the rate of incoming
    writes
    - number of unmerged segments will grow on disk until run out of space
    - reads also slow down - need to check more segments
- Advantage of B-tree:
  - each key exists exactly one place in the index
  - a log-structured db engine may have multiple copies of the same key in
    different segments
  - used in DB with strong transactional semantics

### Other indexing structures

- **primary key**
- common to have **secondary indexes**
  - created by `CREATE INDEX`
  - performing joins efficiently
- secondary index can easily be constructed from a key-value index
- difference:
  - in a secondary index, the indexed values are not necessarily unique

#### Storing values within the index

- the key in an index is the thing that queries search for
  - **clustered index**
- the value can be one of the two:
  - the actual row (document, vertex)
  - a reference to the row stored elsewhere
    - **heap file**: where rows are stored
    - stores data in _no_ particular order
    - avoids duplicating data when multiple secondary indexes are present
    - efficient when updating a value _without_ changing the key
      - overwritten in place
      - assuming new value is not larger than old
- **clustered index**
  - the extra hop from index to heap is too much for reads
  - store the row directly within an index
  - in MySQL,
    - primary key is _always_ a clustered index
    - secondary indexes refer to the primary key
- A compromise between clustered index and non-
  - **covering index** or **index with included columns**
  - stores _some_ columns within index

#### Multi-column indexes

- **concatenated index**: most common
  - combines several fields into one key by appending one column to another
- multi-dimensional indexes are more general to query several columns at once
- specialized spatial indexes such as **R-tree**

#### Full-text search and fuzzy indexes

- Lucene uses a SSTable-like structure for the term dictionary
  - requires a small in-memory index
  - a finite automation over the characters in the keys, similar to **trie**
  - the automation can be transformed into a **Levenshtein automation**
- in LevelDB, the in-memory index is a sparse collection of some of the keys

#### Keep everything in memory

- **in-memory db**
- Memcached
- RAMCloud
- Redis: weak durability by writing to disk asynchronously
- OS caches recently used disk blocks in memory anyway
- in-memory dbs are faster because they can avoid the overheads of encoding in-memory data structures in a form that can
  be written to disk
- provides data models that are difficult to implement with disk-based indexes
  - priority queue
  - set
- **anti-caching**
- **NVM (non-volatile memory)**

### Transaction processing vs. Analytics

- **ACID**:
  - atomicity
  - consistency
  - isolation
  - durability
- transaction processing:
  - allow clients to make low-latency reads/writes
  - opposed to **batch processing**
- **online transaction processing** (OLTP)
- _data analytics_
  - **online analytic processing** (OLAP)
- **Data warehouse**
  - a separate database, where
  - analysts can query
  - without affecting OLTP operations
  - data copied/extracted from OLTP db
    - via periodic data dump, or
    - continuous stream of updates
  - data is cleaned up and loaded into db warehouse
    - **ETL**: extract-transform-load
- **star schema** (a.k.a. dimensional modeling)
- **snowflake schema**

### Column oriented storage

- don't store all the values from one row together
- store all values from each column together instead
- Column compression:
  - **bitmap encoding**
- **vectorized processing**
  - operators such as bitwise AND and OR can be designed to operate on chunks
    of compressed column data directly
- **materialized aggregates**
  - similar to **virtual view**
- bottlenecks:
  - in OLTP, bottleneck is disk seek time
  - in OLAP, bottleneck is disk bandwidth
