# Data Models and Query Languages

## Relational Model vs. Document Model

- Relational model:
  - that of SQL
  - data organized into **relations** (tables in SQL)
  - where each relation is an unordered collection of tuples (rows in SQL)
- _business data processing_
- _transaction processing_
- _batch processing_
- NoSQL
  - scalability
  - specialized query operations
  - schema
- **polygot persistence**
- **impedance mismatch**
- **ORM** (Object-relational mapping)
- **normalization**
  - remove the duplication caused by entering text strings instead of referring
    to an ID
- many-to-many relationships and joins in relational databases
- the Network model:
  - a.k.a. **CODASYL model**
- **document reference**
- **schema-on-read** vs. **schema-on-write**
  - schema-on-read:
    - structure of the data is implicit and only interpreted when data is read
  - schema-on-write:
    - traditional approach of relational databases
    - schema is explicit
    - database ensures all written data conforms to the schema
- data locality for queries
  - document usually stored as a single continuous string
  - encoded as JSON, XML, or BSON (by MongoDB)
  - if needed to access the entire document, there's a performance advantage to
    the storage locality
- the locality advantage _only_ applies if you need large parts of the document
  at the same time
  - if you only need a small portion of the document, it would be wasteful
- on updates to a document,
  - the entire document usually needs to be rewritten
  - only mods that don't change the encoded size of a document can be performed
    in-place
- generally recommended that
  - you keep documents fairly small
  - avoid writes that increase the size of a document

## Query Languages for Data

- SQL is a **declarative** query language
- advantages of a declarative language:
  - more concise and easier to work with
  - hides implementation details
    - possible for performance improvements
  - parallel execution

### MapReduce

- a programming model for processing large amounts of data in bulk across many
  machines
- a fairly low-level programming model for distributed execution on a cluster
  of machines

## Graph-like Data Models

- graph consists of two elements:
  - vertices
  - edges
- can provide a consistent way of storing completely different types of objects
  in a single datastore
- Several different but related ways of structuring and querying data in graphs
  - **property graph**, in Neo4j, Titan, and Infinite Graph
  - **triple-store**, in Datomic, AllegroGraph
- Declarative query languages for graphs
  - Cypher
  - SPARQL
  - Datalog
- Property graph:
  - vertex:
    - a unique ID
    - set of outgoing edges
    - set of incoming edges
    - collection of properties (key-value pairs)
  - edge
    - unique ID
    - tail vertex
    - head vertex
    - label
    - collection of properties
- Triple-store
  - all info stored in three-part statements:
    - subject - equivalent to a vertex in graph
    - predicate
    - object
- the **semantic web**
- Datalog, a subset of Prolog
  - `predicate(subject, object)`
